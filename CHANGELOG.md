# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.4.10] - 2024-12-04

### Security

-   Update `path-to-regexp` override of `express` to 0.1.11 [CVE-2024-45296](https://github.com/advisories/GHSA-9wv6-86v2-598j)
-   Update `body-parser` override of `express` to 1.20.3 [CVE-2024-45590](https://github.com/advisories/GHSA-qwcr-r2fm-qrc7)

### Changed

-   Update Node.js to v20.18.0 ([core#119](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/119))

## [0.4.9] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [0.4.8] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [0.4.7] - 2024-01-22

-   upgrade pino logger

## [0.4.6] - 2023-08-30

### Fixed

-   Fix disabled tasks causing an out of bound array read ([cron-tasks#9](https://gitlab.com/operator-ict/golemio/code/cron-tasks/-/issues/9))

## [0.4.5] - 2023-08-28

### Added

-   Add optional task definition properties `disabled` and `reason` ([cron-tasks#9](https://gitlab.com/operator-ict/golemio/code/cron-tasks/-/issues/9))

## [0.4.4] - 2023-07-31

### Changed

-   Node 18.17

## [0.4.3] - 2023-07-17

### Added

-   Opentelemetry

## [0.4.2] - 2023-07-10

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))
-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [0.4.1] - 2023-04-18

### Changed

-   Set queue type default to quorum ([code#63](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/63))

## [0.4.0] - 2023-03-25

### Fixed

-   Update `cron` dependency to v2.3.0 to mitigate https://github.com/kelektiv/node-cron/issues/467

## [0.3.0] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [0.2.0] - 2023-01-23

### Changed

-   Docker image optimization
-   Migrate to npm

## [0.1.2] - 2022-11-29

### Changed

-   use pino logger instead of winston

## [0.1.1] - 2022-09-21

### Changed

-   Update Node.js to 16.17.0 ([code/general#418](https://gitlab.com/operator-ict/golemio/code/general/-/issues/418)

### Removed

-   Unused dependencies ([modules/general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [0.1.0] - 2022-05-04

### Added

-   alternate exchange for dataplatform exchange

