"use strict";

import "mocha";
import { App } from "../src/App";
import { config } from "../src/config";

const chai = require("chai");
const expect = chai.expect;
const chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);

describe("App", () => {

    it("should start", async () => {
        const app = await new App().start();
        expect(app).to.be.undefined;
    });

    it("should have all config variables set", () => {
        expect(config).not.to.be.undefined;
        expect(config.rabbit_connection).not.to.be.undefined;
    });

    it("should have all enabled tasks started", async () => {
        const app = new App();
        await app.start();
        const running = app.getRunningTasks();
        const stopped = app.getStoppedTasks();
        const runningTaskNames = running.map(({ name }) => name);
        expect(running).to.have.lengthOf(2);
        expect(stopped).to.be.an("array").that.is.empty;
        expect(runningTaskNames).to.include("0 * * * * *: cron.EXCHANGE.DATASET.refreshDataInDB");
        expect(runningTaskNames).to.include("0 */5 * * * *: cron.EXCHANGE.DATASET.refreshDataInDB");
    });

});
