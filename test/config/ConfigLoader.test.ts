/// <reference path="../../node_modules/@types/node/index.d.ts" />

"use strict";

import "mocha";
import { config, ConfigLoader } from "../../src/config";

const chai = require("chai");
const expect = chai.expect;
const chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);

describe("ConfigLoader", () => {

    it("should load config from file", () => {
        const conf = () => new ConfigLoader("cronTasks");
        expect(conf).to.not.throw();
    });

    it("should throws Error if config file is not found", () => {
        const conf = () => new ConfigLoader("test");
        expect(conf).to.throw();
    });

    it("should properly load all configurations", () => {
        // datasources config file
        expect(config.cronTasks).is.not.null;
        // env variables
        expect(config.node_env).is.not.null;
    });

});
