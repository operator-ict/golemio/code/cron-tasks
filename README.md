# Golemio Cron Tasks

Cron Tasks of the Golemio Data Platform System.

Developed by http://operatorict.cz

## Docker instalation

### Prerequisites

-   Docker Engine
-   RabbitMQ

### Instalation

1. Build docker image by `docker build -t cron-tasks .`
2. Setup ENV variables by `.env` file or add `-e VAR=VALUE` to docker run
3. Create file `config/cronTasks.json`.
4. Run container by

```
docker run --rm \
    -e RABBIT_CONN="amqp://rabbit:password@rabbit-service" \ # connection string for rabbitmq
    -v config/cronTasks.json:/home/node/app/config/cronTasks.json:ro
    cron-tasks # docker image label (defined by step 1)
```

## Local instalation

### Prerequisites

-   node.js
-   RabbitMQ
-   npm
-   typescript

### Installation

Install Node

Install all npm modules using command:

```
npm install
```

### Configuration

Configuration is split to environment (.env file) options and cron tasks definitons.

The cron tasks definitons are in the `config/` directory. By default in the directory is only template of tasks (file `cronTasks.template.json`). If you want to override the default definitions, you can create the file `cronTasks.json` with tasks.

Environment options can be set with the system (e.g. in debian with `export NODE_ENV=test`) or with the `.env` file. Example of the `.env` file is saved as `.env.template`.

### Build & Run

#### Production

To compile typescript code into js one-time (production build):

```
npm run build
```

To run the app:

```
npm start
```

#### Dev/debug

Run via TypeScript (in this case it is not needed to build separately, application will watch for changes and restart on save):

```
npm run dev-start
```

or run with a debugger:

```
npm run dev-start-debug
```

Runing the application in any way will load all config variables from environment variables or the .env file. To run, set all environment variables from the `.env.template` file, or copy the `.env.template` file into new `.env` file in root directory and set variables there.

Project uses `dotenv` package: https://www.npmjs.com/package/dotenv

### Cron Jobs

Cron-tasks module only send messages to RabbitMQ in specified time. Because cron-tasks module has no permissions to create the queues binded to exchange, by default exchange receive the message and discard it.

If you want to store the messages in the queue, you must use `golemio integration engine` or manually create testing queue and bind it with exchange. For creating the testing queues you can use [RabbitMQ Management UI](https://www.rabbitmq.com/management.html) ([manual](https://www.cloudamqp.com/blog/2015-05-27-part3-rabbitmq-for-beginners_the-management-interface.html)).

## Tests

To run all test defined in /test directory simply run this command:

```
npm test
```

from the application's root directory.

## Logging

Logging uses `pino` for standard logging with levels and `debug` (https://www.npmjs.com/package/debug) for debugging.

All logs with `silly` and `debug` level are printed as standard log (if appropriate log level is set) using `pino` as well as using `debug` module with `"golemio:cron-tasks:silly"` or `"golemio:cron-tasks:debug"` settings.

You can set both `LOG_LEVEL` and `DEBUG` settings in ENV variables.

## Documentation

For generating documentation run `npm run generate-docs`. TypeDoc source code documentation is located in `docs/typedoc`.

## Contribution guidelines

Please read `CONTRIBUTING.md`.

## Troubleshooting

Contact benak@operatorict.cz or vycpalek@operatorict.cz
