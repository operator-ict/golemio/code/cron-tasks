"use strict";

import * as path from "path";

/// Path to config files directory
const FILES_DIR = "../../config/";
/// Config files extension
const FILES_EXT = ".json";

/**
 * Helper for loading and merging default and specific config files.
 */
export class ConfigLoader {
    /** Object with configurations */
    public conf: any;

    /**
     * Constructor
     *
     * @param {string} filename Filename of the configuration file.
     */
    constructor(filename: string) {
        try {
            this.conf = require(path.join(__dirname, FILES_DIR, filename + FILES_EXT));
        } catch (err) {
            this.conf = require(path.join(__dirname, FILES_DIR, filename + ".template" + FILES_EXT));
        }
    }
}

/** Exporting all configurations */
export const config = {
    app_name: process.env.APP_NAME || "cron-tasks",
    app_version: process.env.npm_package_version,
    colorful_logs: process.env.COLORFUL_LOGS,
    cronTasks: new ConfigLoader("cronTasks").conf,
    log_level: process.env.LOG_LEVEL,
    node_env: process.env.NODE_ENV,
    rabbit_connection: process.env.RABBIT_CONN,
};
