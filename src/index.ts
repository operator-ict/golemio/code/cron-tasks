// Load telemetry before all deps - instrumentation patches the libs on load
import { initTraceProvider } from "@golemio/core/dist/monitoring";
initTraceProvider();

import { App } from "./App";

new App().start();
