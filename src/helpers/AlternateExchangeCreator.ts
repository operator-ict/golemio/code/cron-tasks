import { Channel } from "amqplib";

export default class AlternateExchangeCreator {
    private static EXCHANGE_NAME_SUFFIX = "-alt";
    private static EXCHANGE_QUEUE_NAME = ".alt-routes";

    public static createAlternateExchange = async (channel: Channel, originalExchangeName: string): Promise<void> => {
        const altExchangeName = AlternateExchangeCreator.getAltExchangeName(originalExchangeName);
        await channel.assertExchange(altExchangeName as string, "fanout", {
            durable: false,
        });

        const q = await channel.assertQueue(altExchangeName + AlternateExchangeCreator.EXCHANGE_QUEUE_NAME, {
            arguments: { "x-queue-type": "quorum" },
            durable: true,
            messageTtl: 3 * 24 * 60 * 60 * 1000, // 3 days in milliseconds
        });

        channel.bindQueue(q.queue, altExchangeName, "dead");
    };

    public static getAltExchangeName = (originalExchangeName: string): string => {
        return originalExchangeName.concat(AlternateExchangeCreator.EXCHANGE_NAME_SUFFIX);
    };
}
