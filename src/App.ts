"use strict";

import { ErrorHandler, GeneralError } from "@golemio/errors";
import { Options } from "amqplib";
import { CronJob } from "cron";
import { config } from "./config";
import { AMQPConnector } from "./connectors";
import { log } from "./helpers";
import AlternateExchangeCreator from "./helpers/AlternateExchangeCreator";

/**
 * Model for containing all cron jobs. Provides methods to start them, stop them or monitor them.
 */
export class App {
    private activeCrons: Array<{ name: string; job: CronJob }>;

    constructor() {
        this.activeCrons = [];
    }

    public async start(): Promise<void> {
        try {
            await AMQPConnector.connect();

            this.registerTasks();

            this.startAll();
            log.info("Started!");
            this.checkStatus();
        } catch (err) {
            ErrorHandler.handle(err, log);
        }
    }

    // Start all cron jobs
    public startAll(): void {
        for (let i = 0, imax = this.activeCrons.length; i < imax; i++) {
            this.activeCrons[i].job.start();
        }
    }

    // Stop all cron jobs
    public stopAll(): void {
        for (let i = 0, imax = this.activeCrons.length; i < imax; i++) {
            this.activeCrons[i].job.stop();
        }
    }

    // Check the status of all current cron jobs, running and stopped both, and print it
    public checkStatus(): void {
        for (let i = 0, imax = this.activeCrons.length; i < imax; i++) {
            const cron = this.activeCrons[i];
            log.info("Cron '" + cron.name + "' is running: " + cron.job.running);
            log.info("-> Next send time: " + cron.job.nextDate().toString());
        }
    }

    public getRunningTasks(): Array<{ name: string; job: CronJob }> {
        return this.activeCrons.filter((item) => item.job.running === true);
    }

    public getStoppedTasks(): Array<{ name: string; job: CronJob }> {
        return this.activeCrons.filter((item) => item.job.running === false);
    }

    private registerTasks(): void {
        for (const task of config.cronTasks) {
            const name = task.cronTime + ": " + task.routingKey;
            if (task.disabled === true) {
                log.warn(`Cron '${name}' is disabled. Reason: '${task.reason ?? ""}'.`);
                continue;
            }
            const job = new CronJob(
                task.cronTime,
                async () => {
                    const message = task.message;
                    await this.sendMessageToExchange(task.exchange, task.routingKey, message, task.options);
                    log.info("Message sent! Key: `" + task.routingKey + "`, message: `" + message + "`");
                    log.verbose("-> Next send time: " + job.nextDate().toString());
                },
                null,
                false,
                "Europe/Prague"
            );
            this.activeCrons.push({ job, name });
        }
    }

    private async sendMessageToExchange(
        exchange: string,
        routingKey: string,
        message: string,
        options?: Options.Publish
    ): Promise<void> {
        try {
            const channel = await AMQPConnector.getChannel();

            AlternateExchangeCreator.createAlternateExchange(channel, exchange);

            channel.assertExchange(exchange, "topic", {
                durable: false,
                alternateExchange: AlternateExchangeCreator.getAltExchangeName(exchange),
            });
            channel.publish(exchange, routingKey, Buffer.from(message), options);
        } catch (err) {
            throw new GeneralError("Sending the message to exchange failed.", "App", err);
        }
    }
}
