"use strict";

import { FatalError, ErrorHandler } from "@golemio/errors";
import * as amqplib from "amqplib";
import { config } from "../config";
import { log } from "../helpers";

class MyAMQP {
    private channel: amqplib.Channel;

    public connect = async (): Promise<amqplib.Channel | undefined> => {
        try {
            if (this.channel) {
                return this.channel;
            }

            const conn = await amqplib.connect(config.rabbit_connection);
            this.channel = await conn.createChannel();
            log.info("Connected to Queue!");
            conn.on("close", () => {
                ErrorHandler.handle(new FatalError("Queue disconnected"), log);
            });
            return this.channel;
        } catch (err) {
            throw new FatalError("Error while creating AMQP Channel.", this.constructor.name, err);
        }
    };

    public getChannel = (): amqplib.Channel => {
        if (!this.channel) {
            throw new FatalError("AMQP channel not exists. First call connect() method.", this.constructor.name);
        }
        return this.channel;
    };
}

const AMQPConnector = new MyAMQP();

export { AMQPConnector };
